package com.talat.pinterest.pinboard;

import com.talat.pinterest.data.PinboardRepository;
import com.talat.pinterest.data.models.Board;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;

/**
 *Test Class for Pinboard Presenter.
 */
public class PinboardPresenterTest {

    private PinboardPresenter presenter;
    @Mock
    private PinboardContract.View view;
    @Mock
    private PinboardRepository repository;
    @Captor
    private ArgumentCaptor<PinboardRepository.PinboardCallBack>  pinboardCallBackArgumentCaptor;

    private List<Board> pins;



    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        presenter = new PinboardPresenter(view, repository);
        pins = new ArrayList<>();
        pins.add(new Board());
    }



    @Test
    public void loadBoard_shouldReturnNoErrors(){
        presenter.loadBoard();
        verify(view).setLoadingIndicator(true);
        verify(repository).getBoard(pinboardCallBackArgumentCaptor.capture());
        pinboardCallBackArgumentCaptor.getValue().onSuccess(pins);
        verify(view).showBoard(pins);
        verify(view).setLoadingIndicator(false);

    }

    @Test
    public void loadBoard_shouldReturnErrorLoading(){
        presenter.loadBoard();
        verify(view).setLoadingIndicator(true);
        verify(repository).getBoard(pinboardCallBackArgumentCaptor.capture());
        pinboardCallBackArgumentCaptor.getValue().onError("Error Loading");
        verify(view).showErrorLoadingBoard();
        verify(view).setLoadingIndicator(false);

    }


}
