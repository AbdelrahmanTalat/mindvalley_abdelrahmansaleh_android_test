package com.talat.pinterest.data;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.talat.pinterest.data.models.Board;
import com.talat.pinterest.util.Constants;
import com.talat.resourceloader.ResourceLoader;
import com.talat.resourceloader.data.Resource;
import com.talat.resourceloader.data.ResourceCallback;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Data repository for pins.
 */
public class PinboardRepository {

    public PinboardRepository() {
    }

    public void getBoard(final PinboardCallBack callBack) {
        ResourceLoader.getInstance().load(Constants.SERVICE_URL, new ResourceCallback() {
            @Override
            public void onSuccess(Resource response) {
                String jsonString = response.toUtf8String();
                Type type = new TypeToken<List<Board>>() {
                }.getType();
                Gson gson = new Gson();
                callBack.onSuccess((List<Board>) gson.fromJson(jsonString, type));

            }

            @Override
            public void onError(String errorMessage) {
                callBack.onError(errorMessage);

            }
        });
    }

    /**
     * Interface for the repository request.
     */
    public interface PinboardCallBack {
        void onSuccess(List<Board> pins);

        void onError(String message);
    }
}
