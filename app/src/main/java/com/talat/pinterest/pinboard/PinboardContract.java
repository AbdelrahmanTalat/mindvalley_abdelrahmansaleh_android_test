package com.talat.pinterest.pinboard;

import com.talat.pinterest.data.models.Board;

import java.util.List;

/**
 * Created by talat on 06-08-2016.
 */
public interface PinboardContract {

    interface View {
        void showBoard(List<Board> pins );
        void setLoadingIndicator(boolean active);
        void showErrorLoadingBoard();
        void setPresenter(Presenter presenter);


    }

    interface Presenter{
        void loadBoard();

    }

}
