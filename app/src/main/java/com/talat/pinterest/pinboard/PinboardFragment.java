package com.talat.pinterest.pinboard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.talat.pinterest.R;
import com.talat.pinterest.data.models.Board;
import com.talat.pinterest.libs.RecyclerWithEmptyView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by talat on 06-08-2016.
 */
public class PinboardFragment extends Fragment implements PinboardContract.View {

    private static final String TAG = "PinboardFragment";
    PinboardContract.Presenter pinboardPresenter;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    @BindView(R.id.rvPins)
    RecyclerWithEmptyView rvGrid;

    public PinboardFragment() {
    }

    public static PinboardFragment getInstance() {
        return new PinboardFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_pinboard, container, false);
        ButterKnife.bind(this, root);
        setupSwipeContainer();
        setHasOptionsMenu(true);
        return root;
    }

    private void setupSwipeContainer() {
        swipeContainer.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pinboardPresenter.loadBoard();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        pinboardPresenter.loadBoard();
    }

    @Override
    public void showBoard(List<Board> pins) {

        if (pins != null && pins.size() > 0) {
            Log.d(TAG, pins.size() + "");
            rvGrid.setHasFixedSize(true);
            StaggeredGridLayoutManager _staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            rvGrid.setLayoutManager(_staggeredGridLayoutManager);
            PinsRecyclerAdapter rcAdapter = new PinsRecyclerAdapter(getContext(), pins);
            rvGrid.setAdapter(rcAdapter);

        }

    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (swipeContainer != null) {
            swipeContainer.post(new Runnable() {
                @Override
                public void run() {
                    swipeContainer.setRefreshing(active);
                }
            });
        }

    }

    @Override
    public void showErrorLoadingBoard() {


    }

    @Override
    public void setPresenter(PinboardContract.Presenter presenter) {
        pinboardPresenter = presenter;
    }
}
