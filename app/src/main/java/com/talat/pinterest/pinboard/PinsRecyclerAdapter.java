package com.talat.pinterest.pinboard;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.talat.pinterest.R;
import com.talat.pinterest.data.models.Board;
import com.talat.pinterest.data.models.Category;
import com.talat.resourceloader.ResourceLoader;
import com.talat.resourceloader.data.Resource;
import com.talat.resourceloader.data.ResourceCallback;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * RecyclerAdapter for Grid items in the pinboard.
 */
public class PinsRecyclerAdapter extends RecyclerView.Adapter<PinsRecyclerAdapter.PinViewHolder> implements View.OnClickListener, View.OnLongClickListener {
    private List<Board> pins;
    private Context context;
    private int lastPosition = -1;

    public PinsRecyclerAdapter(Context context, List<Board> bins) {
        this.context = context;
        this.pins = bins;
    }


    @Override
    public PinViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View _Item = LayoutInflater.from(parent.getContext()).inflate(R.layout.pin_item_layout, parent, false);
        return new PinViewHolder(_Item);
    }

    private void updatePinImage(PinViewHolder holder, Bitmap bitmap) {
        holder.btnCancel.setVisibility(View.GONE);
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(holder.itemView.getContext(), R.anim.fade_in_quick);
        holder.pinImage.startAnimation(myFadeInAnimation);
        holder.pinImage.setImageBitmap(bitmap);
    }

    private void updateProfilePic(PinViewHolder holder, Bitmap bitmap) {
        holder.profileImage.setImageBitmap(bitmap);
    }

    @Override
    public void onBindViewHolder(final PinViewHolder holder, int position) {
        Board board = pins.get(position);
        int totalPics = 0;
        if (board.getUser() != null) {
            holder.userName.setText("@" + board.getUser().getUsername());
            holder.fullName.setText(board.getUser().getName());
            holder.userName2.setText(board.getUser().getUsername().length() > 10 ? (board.getUser().getUsername().substring(0, 10)) + "..."
                    : board.getUser().getUsername());
        }

        ResourceLoader.getInstance().load(board.getUrls().getSmall(), new ResourceCallback() {
            @Override
            public void onSuccess(Resource response) {
                updatePinImage(holder, response.toBitmap());
            }

            @Override
            public void onError(String errorMessage) {

            }
        });

        ResourceLoader.getInstance().load(board.getUser().getProfileImage().getSmall(), new ResourceCallback() {
            @Override
            public void onSuccess(Resource response) {
                updateProfilePic(holder, response.toBitmap());
            }

            @Override
            public void onError(String errorMessage) {

            }
        });

        holder.selfLiked.setText(board.getLikedByUser() ? "Yes" : "No");
        holder.totalLikes.setText(String.valueOf(board.getLikes()));

        if (board.getCategories() != null) {
            for (Category category : board.getCategories()) {
                totalPics += category.getPhotoCount();
            }
        }

        holder.totalPics.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.ENGLISH).format(totalPics)));
        setAnimation(holder.itemView.getRootView(), position);
        holder.itemView.setTag(board);
    }

    @Override
    public int getItemCount() {
        return pins.size();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onLongClick(View v) {
        Toast.makeText(context, "Long press triggered", Toast.LENGTH_LONG).show();
        return true;
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public class PinViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView userName;
        public TextView fullName;
        public ImageView pinImage;
        public TextView totalPics;
        public TextView totalLikes;
        public TextView selfLiked;
        public TextView userName2;
        public CircleImageView profileImage;
        public ImageButton btnCancel;

        public PinViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            userName = (TextView) itemView.findViewById(R.id.pin_item_userName);
            fullName = (TextView) itemView.findViewById(R.id.pin_item_fullName);
            pinImage = (ImageView) itemView.findViewById(R.id.pin_item_image);
            totalPics = (TextView) itemView.findViewById(R.id.pin_item_totalPics);
            totalLikes = (TextView) itemView.findViewById(R.id.pin_item_likes);
            selfLiked = (TextView) itemView.findViewById(R.id.pin_item_self_liked);
            userName2 = (TextView) itemView.findViewById(R.id.pin_item_userName2);
            profileImage = (CircleImageView) itemView.findViewById(R.id.imgAuthorPic);
            btnCancel = (ImageButton) itemView.findViewById(R.id.btn_cancel);
        }

        @Override
        public void onClick(View view) {

        }

        @Override
        public boolean onLongClick(View v) {
            Toast.makeText(v.getContext(), "Long clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
