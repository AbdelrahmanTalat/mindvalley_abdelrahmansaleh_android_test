package com.talat.pinterest.pinboard;

import android.support.annotation.NonNull;

import com.talat.pinterest.data.PinboardRepository;
import com.talat.pinterest.data.models.Board;

import java.util.List;

/**
 * Created by talat on 06-08-2016.
 */
public class PinboardPresenter implements PinboardContract.Presenter {

    private PinboardContract.View pinboardView;
    private PinboardRepository pinboardRepository;

    public PinboardPresenter(@NonNull PinboardContract.View pinboardView,@NonNull PinboardRepository pinboardRepository) {
        this.pinboardView = pinboardView;
        this.pinboardRepository = pinboardRepository;
        this.pinboardView.setPresenter(this);
    }

    @Override
    public void loadBoard() {
        pinboardView.setLoadingIndicator(true);
        pinboardRepository.getBoard(new PinboardRepository.PinboardCallBack() {
            @Override
            public void onSuccess(List<Board> pins) {
                pinboardView.showBoard(pins);
                pinboardView.setLoadingIndicator(false);

            }

            @Override
            public void onError(String message) {
                pinboardView.showErrorLoadingBoard();
                pinboardView.setLoadingIndicator(false);
            }
        });

    }
}
