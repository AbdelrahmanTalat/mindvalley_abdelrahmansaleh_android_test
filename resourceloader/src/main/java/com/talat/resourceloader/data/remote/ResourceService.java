package com.talat.resourceloader.data.remote;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Interface for retrofit.
 */
public interface ResourceService {

    @GET
    Call<ResponseBody> getResource(@Url String url );

}
