package com.talat.resourceloader.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import java.io.UnsupportedEncodingException;

/**
 * Data type .
 */
public class Resource {


    private byte[] data;

    public Resource(@NonNull byte[] data) {
        this.data = data;
    }

    /**
     * Converts ({@link Resource}) to a Bitmap.
     */
    public Bitmap toBitmap() {
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }

    /**
     * Converts ({@link Resource}) to a UTF-8 String.
     */
    public String toUtf8String() {
        try {
            String str = new String(data, "UTF-8");
            return str;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
