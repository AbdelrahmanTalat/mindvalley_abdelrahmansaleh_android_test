package com.talat.resourceloader.data;

/**
 * Created by talat on 05-08-2016.
 */
public interface ResourceCallback {
    void onSuccess(Resource response);
    void onError (String errorMessage);
}
