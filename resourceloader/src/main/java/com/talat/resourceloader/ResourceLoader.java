package com.talat.resourceloader;

import android.util.Log;

import com.talat.resourceloader.data.Resource;
import com.talat.resourceloader.data.ResourceCallback;
import com.talat.resourceloader.data.remote.ResourceService;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A thread safe singleton class to load resources.
 */
public class ResourceLoader {

    private static class Loader{
        static volatile ResourceLoader INSTANCE = new ResourceLoader();
    }

    private ResourceLoader() {
    }

    public static ResourceLoader getInstance() {
        return Loader.INSTANCE;
    }

    public void load(String url, final ResourceCallback callback){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://your.api-base.url")
                .client( new OkHttpClient().newBuilder().build())
                .build();
        ResourceService service = retrofit.create(ResourceService.class);
        Call<ResponseBody> call = service.getResource(url);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Resource resource = new Resource(response.body().bytes());
                    callback.onSuccess(resource);
                } catch (IOException e) {
                    callback.onError(e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });


    }




}
